'''
@author: JanRichter
Huge thanks to Ben Tanen for explaining the FGK algorithm! http://ben-tanen.com/adaptive-huffman/
'''

class Node:
    """Represents a single node in a binary tree used for Adaptive Huffman algorithm.
    
    Attributes:
    parent : Node or None
        parent node of the node in binary tree structure
    left_child : Node or None
        left child node of the node in binary tree structure
    right_child : Node or None
        right child node of the node in binary tree structure
    symbol: string or None
        symbol that the Node is representing, could be None for a chain node in Adaptive Huffman algorithm
    symbol_amount: integer
        weight of a node
    """
    def __init__(self, parent, symbol):
        self.parent = parent
        self.left_child = None # -> 0
        self.right_child = None # -> 1
        self.symbol = symbol 
        self.symbol_amount = 0
        
        
class AdaptiveHuffman:
    """A class used for encoding and decoding of input strings by the Adaptive Huffman algortihm by Faller-Gallager-Knuth.
    
    Attributes:
    tree_root : Node
        root node of the binary tree
    zero_node : Node
        ZERO node in the binary tree
    node_list : list
        list of all nodes in the binary tree
    symbol_set : set
        set of all symbols used in binary tree
    node_code_output : string
        logs all symbols with their specific codes for each turn of the algorithm
    output : string
        encoded or decoded input string
    """
    
    def __init__(self):
        self.tree_root = Node(parent=None, symbol='ZERO')
        self.zero_node = self.tree_root
        self.node_list = [self.zero_node]
        self.symbol_set = set()
        self.node_code_output = ''
        self.output = ''
        
    def add_one_to_amount_up(self, node):
        """Adds +1 symbol amount to specific node and to its parent up to the binary tree root.
        
        Parameters:
        node : Node
        """
        node.symbol_amount += 1
        if node.symbol != None:
            print('Adding +1 symbol amount to node with symbol ' + node.symbol + '.')
        else:
            print('Adding +1 symbol amount to chain node.')
        if node.parent != None:
            self.add_one_to_amount_up(node.parent) 
     
    def append_to_node_code_output(self, turn_number):
        """Appends the current codes for each symbol into global node code output string.
        
        Parameters:
        turn_number : integer
        """
        self.node_code_output += 'Node codes after char no. ' + str(turn_number) + ':\n'
        for node in self.node_list:
            if node.symbol in self.symbol_set or node.symbol == 'ZERO':
                self.node_code_output += node.symbol + '\t' + self.get_code(node) + '\n'
        
    def append_to_output(self, text, add_space = True):
        """Appends specific string into the global output string.
        
        Parameters:
        text : string
            input string
        add_space : bool
            if set as True, appended text will be followed by the space character
        """
        print('Appending ' + text + ' to output.')
        if add_space:
            self.output += text + ' '
        else:
            self.output += text
    
    def check_tree(self):
        """Compares all the nodes in the binary tree with themselves and swaps the nodes that breaks the sibling property of a binary tree.
        If nodes from different depths are swapped, the process repeats from the beginning.
        """
        swapped_different_depths = False
        for node in self.node_list:
            for node2 in self.node_list:
                # Check sibling property for nodes in different depths.
                if node != node2 \
                     and self.get_depth(node) > self.get_depth(node2) \
                     and node.symbol_amount > node2.symbol_amount:
                    self.swap(node, node2)
                    swapped_different_depths = True
                # Check sibling property for nodes in the same depth.
                elif node != node2 and self.get_depth(node) == self.get_depth(node2) \
                     and int(self.get_code(node, no_blank=True), 2) < int(self.get_code(node2, no_blank=True), 2)\
                     and node.symbol_amount > node2.symbol_amount:
                    self.swap(node, node2)
        if swapped_different_depths: 
            self.check_tree()
    
    def get_binary(self, char):
        """Returns ASCII binary value for specific character.
    
        Parameters:
        char : string
        """
        return bin(ord(char)).replace('0b', '')
    
    def get_code(self, node, code = '', no_blank = False):
        """Returns binary code for a specific node in the binary tree.
        
        Parameters:
        node : Node
        code : string
            used for recursive drill through the binary tree
        no_blank : bool
            if set as True, tree root code will be returned as '0' instead of None
        """
        if node.parent is None:
            return '0' if no_blank else code 
        code_addition = '0' if node == node.parent.left_child else '1'
        return self.get_code(node.parent, code_addition + code)
    
    def get_depth(self, node):
        """Returns depth of a specific node in the binary tree.
        
        Parameters:
        node : Node
        """
        return len(self.get_code(node))
    
    def get_node(self, symbol):
        """Lookup for a specific node by its symbol.
        
        Parameters:
        symbol : string
        """
        for node in self.node_list:
            if node.symbol == symbol:
                return node
    
    def get_node_by_code(self, code):
        """Lookup for a specific node by its binary code.
        
        Parameters:
        code: string
        """
        for node in self.node_list:
            if self.get_code(node) == code:
                return node
    
    def print_nodes(self):
        """Prints attributes of all existing nodes in the binary tree.
        """
        for node in sorted(self.node_list, key=lambda node: self.get_depth(node)):
            print('All information about node', node)
            print('Parent:', node.parent, '| Left child:', node.left_child, '| Right child:', node.right_child)
            print('| Symbol:', node.symbol, '| Weight:',  node.symbol_amount)
            print('| Code:',self.get_code(node), '| Depth:', self.get_depth(node), '\n')
    
    def recount_chain_weights(self):
        """Checks through all nodes in the binary tree to check sum mismatches and recounts them.
        If there is any sum mismatch, the process repeats itself from the beginning.
        """
        changed = False
        for node in self.node_list:
            if (node.symbol == None):
                left_amount = node.left_child.symbol_amount if node.left_child.symbol_amount != None else 0
                right_amount = node.right_child.symbol_amount if node.right_child.symbol_amount != None else 0
                if (node.symbol_amount != (left_amount+right_amount)):
                    changed = True
                    node.symbol_amount = left_amount+right_amount
        if (changed): 
            self.recount_chain_weights()
    
    def swap(self, node1, node2):
        """Swaps position of two specific nodes (with their childs). Considers also changes with parents linking to their new children.
        
        Parameters:
        node1 : Node
        node2 : Node
        """
        print('Swapping nodes.')
        node2_original_parent = node2.parent
        if (node1.parent != None):
            if (node1.parent.left_child == node1):
                node1.parent.left_child = node2
            else:
                node1.parent.right_child = node2
        if (node2_original_parent != None):
            if (node2_original_parent.left_child == node2):
                node2_original_parent.left_child = node1
            else:
                node2_original_parent.right_child = node1
        node2.parent = node1.parent
        node1.parent = node2_original_parent
        self.recount_chain_weights()
         
    def encode(self, input_text):
        """Main method - encodes the input string by the adaptive Huffman FGK algorithm. 
        Returns a tuple containing output string and complete node code string.
        
        Parameters:
        input_text : string
        """
        turn_number = 0
        for c in list(input_text):
            turn_number += 1
            if c not in self.symbol_set:
                # Add new symbol into the binary tree.
                self.append_to_output(self.get_code(self.zero_node))
                self.append_to_output(self.get_binary(c))
                if self.zero_node == self.tree_root:
                    new_chain_node = Node(parent=None, symbol=None)
                    self.tree_root = new_chain_node
                else:
                    new_chain_node = Node(parent=self.zero_node.parent, symbol=None)
                    new_chain_node.parent.left_child = new_chain_node
                new_chain_node.left_child = self.zero_node
                new_chain_node.right_child = Node(parent=new_chain_node, symbol=c)
                self.zero_node.parent = new_chain_node
                self.symbol_set.add(c)
                self.node_list.append(new_chain_node)
                self.node_list.append(new_chain_node.right_child)
                self.check_tree()
                self.add_one_to_amount_up(new_chain_node.right_child)
                self.recount_chain_weights()
                self.check_tree()
            else:
                # Increase the symbol amount for specific node and its parents up to the root.
                node_with_symbol = self.get_node(c)
                self.append_to_output(self.get_code(node_with_symbol))
                self.check_tree()
                self.add_one_to_amount_up(node_with_symbol)
                self.recount_chain_weights()
                self.check_tree()
            self.append_to_node_code_output(turn_number)
        self.print_nodes()
        return self.output, self.node_code_output
    
    def decode(self, input_text):
        """Main method - decodes the input string by the adaptive Huffman FGK algorithm. 
        Returns a tuple containing output string and complete node code string.
        
        Parameters:
        input_text : string
        """
        loaded_zero = False
        turn_number = 0
        for c in input_text.split(' '):
            turn_number += 1
            if (self.get_code(self.zero_node) == c):
                loaded_zero = True
            elif loaded_zero:
                # If previous character was ZERO, this character is a new one - add it to the tree.
                loaded_zero = False
                ascii_char = chr(int(c, 2))
                self.append_to_output(ascii_char, add_space=False)
                if self.zero_node == self.tree_root:
                    new_chain_node = Node(parent=None, symbol=None)
                    self.tree_root = new_chain_node
                else:
                    new_chain_node = Node(parent=self.zero_node.parent, symbol=None)
                    new_chain_node.parent.left_child = new_chain_node
                new_chain_node.left_child = self.zero_node
                new_chain_node.right_child = Node(parent=new_chain_node, symbol=ascii_char)
                self.zero_node.parent = new_chain_node
                self.symbol_set.add(ascii_char)
                self.node_list.append(new_chain_node)
                self.node_list.append(new_chain_node.right_child)
                self.check_tree()
                self.add_one_to_amount_up(new_chain_node.right_child)
                self.recount_chain_weights()
                self.check_tree()
            elif(c != ''):
                # Increase the symbol amount for specific node and its parents up to the root.
                node_with_symbol = self.get_node_by_code(c)
                if (node_with_symbol is not None):
                    self.append_to_output(node_with_symbol.symbol, add_space=False)
                    self.check_tree()
                    self.add_one_to_amount_up(node_with_symbol)
                    self.recount_chain_weights()
                    self.check_tree()
            self.append_to_node_code_output(turn_number)
        self.print_nodes()
        return self.output, self.node_code_output