'''
@author: JanRichter
'''

import AdaptiveHuffman
from tkinter import *
from tkinter import scrolledtext

class AdaptiveHuffmanGUI:
    """Class to provide basic GUI for the Adaptive Huffman FGK algorithm.
    
    Parameters: 
    root : tkinter.Tk
        main instance of the Tkinter GUI module
    input_entry : tkinter.scrolledtext.ScrolledText
        input form
    output_entry : tkinter.scrolledtext.ScrolledText
        output form
    node_log: tkinter.scrolledtext.ScrolledText
        form that lists all symbols with codes for each turn of the algorithm
    """
    def __init__(self, root):
        self.root = root
        self.root.title("Adaptive Huffman compression - FGK algorithm; Implemented by Jan Richter, 2018")
        self.root.lift()
        frame = Frame(root)
        frame.pack()
        self.input_entry = scrolledtext.ScrolledText(frame, height=15)
        self.input_entry.grid(row=1, column=1, columnspan=20)
        self.output_entry = scrolledtext.ScrolledText(frame, height=15, background='#D3D3D3')
        self.output_entry.grid(row=2, column=1, columnspan=20)
        self.node_log = scrolledtext.ScrolledText(frame, width=35,height=30, background='#D3D3D3')
        self.node_log.grid(row=1, rowspan=2, column=21, columnspan=5)
        Button(frame, text='Encode', command=self.encode).grid(row=3, column=24)
        Button(frame, text='Decode', command=self.decode).grid(row=3, column=25)
        root.mainloop()
    
    def refresh_ui(self, output, node_log):
        """Removes the current contents of output entry and node log entry, and replaces them with new one.
        
        Parameters:
        output : string
            new content for the output entry
        node_log : string
            new content for the node log entry
        """
        self.output_entry.delete('1.0', END)
        self.output_entry.insert('1.0', output)
        self.node_log.delete('1.0', END)
        self.node_log.insert('1.0', node_log)
        self.node_log.see(END)
    
    def encode(self):
        """Reaction to the Encode button press, encodes the text in the input entry.
        The result is presented in the output entry and node log entry.
        """
        inputText = str(self.input_entry.get("1.0", 'end-1c'))
        output, node_log = AdaptiveHuffman.AdaptiveHuffman().encode(inputText)
        self.refresh_ui(output, node_log)
         
    def decode(self):
        """Reaction to the Decode button press, decodes the binary text in the input entry.
        The result is presented in the output entry and node log entry.
        """   
        inputText = str(self.input_entry.get("1.0", 'end-1c'))
        output, node_log = AdaptiveHuffman.AdaptiveHuffman().decode(inputText)
        self.refresh_ui(output, node_log)

AdaptiveHuffmanGUI(Tk())